# bible-api
[![](https://img.shields.io/matrix/my.awesomebible:matrix.org?color=blueviolet&label=Chat&logo=matrix&cacheSeconds=3600)](https://matrix.to/#/#my.awesomebible:matrix.org)

Eine API die MyBible-Module verarbeitet und bereitstellt.

## Benutzung


## Bibelmodule
Einige Bibelmodule können [hier](https://benjaminwolkchen.github.io/mybible-modules/) gefunden werden.